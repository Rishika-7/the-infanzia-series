# Interactive Learning Applications for Kids: THE INFANZIA SERIES


“The Infanzia Series” is a series of interactive learning mobile applications for pre-schlooers and kindergartners. The main idea behind these applications is to create a fun and interactive learning environment for kids.

### TARGET AUDIENCE : PRE-SCHOOLERS AND KINDERGARTNERS
<img src="https://kidsinadelaide.com.au/wp-content/uploads/2019/08/Ready-Steady-Go-Kids-Adelaide.jpg" width = 50%>


### SERIES CONTENT

The Infanzia Series will contain individual applications for areas which are as follows:

- English and Language : word-it-kid
- Numbers and Math : count-it-kid
- Environmental Studies : know-it-kid

<img src="https://tadvancesite.com/wp-content/uploads/2019/04/Art-Education-for-kids.jpg" width = 50%>

### BASIC STRUCTURE


> **CONTENT DIVISION**

Based on the age group, we have divided the content into two categories, i.e., Pre-Schoolers and Kindergartners.  The content, including the exercises, will be designed in such a way that it goes well with the child and his age.

**We have divided the users into 2 groups and have designed our content based on it.**

1. **Group 1 -> PRESCHOOLERS**

Children are at the very basic stage of their learning now. Many of them would have started with basic reading and writing. Content will include videos like rhymes, alphabets, numbers and colors, so they can get started easily.

2. **Group 2 -> KINDERGARTNER**

Kids of this age group are capable of learning advanced ideas because they can recognize similar patterns. So for them we have decided on a simple quiz and content appropriate for their age like short stories, differentiating objects etc.


> **REDIRECTION AND CONTENT**

Upon login the user will be asked the kid’s name and the age. They’ll be prompted to two different interfaces based on the group selected. 
After selecting the group the kids will be directed to the main page.The main page will be divide into three parts : 

**EDUCATIONAL CONTENT**

The kids will go through video content and then they’ll be prompted to games like quizzes to imbibe their learning. We will also be providing them with markings as they complete different levels.

- **VIDEOS**
We will be integrating videos for both the age groups individually. We are planning to get the content from YouTube and integrate it into our application.
- **QUIZ**
Once a student completes a certain topic, they can take a quiz which would contain questions from that specific topic for each age group. After successfully completing it we will be having a score board, and they will receive a marking.

**GAMES**

To make it a fun learning experience, we have decided to keep certain games that are educational and relevant to the subject. They will be appropriate for all the age groups using the app. 

**RHYMES**

Rhymes are one of most memorable parts of early learning. In order to make this learning process memorable and fun, we will be including certain rhymes and poems for both age groups, combined.

<hr>