import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math';


var AlphaList=[
  {"alpha" : "A", "I" : "Images/ant.png"},
  {"alpha" : "B", "I" : "Images/Bee.png"},
  {"alpha" : "C", "I" : "Images/cat.png"},
  {"alpha" : "D", "I" : "Images/dog.png"},
  {"alpha" : "E", "I" : "Images/eagle.png"},
  {"alpha" : "F",  "I" : "Images/fish.png"},
  {"alpha" : "G",  "I" : "Images/girrafe.png"},
  {"alpha" : "H",  "I" : "Images/Horse.png"},
  {"alpha" : "I",  "I" : "Images/igloo.png"},
  {"alpha" : "J",  "I" : "Images/Jackal.png"},
  {"alpha" : "K",  "I" : "Images/Kangaroo.png"},
  {"alpha" : "L",  "I" : "Images/lion.png"},
  {"alpha" : "M",  "I" : "Images/monkey.png"},
  {"alpha" : "N",  "I" : "Images/nest.png"},
  {"alpha" : "O",  "I" : "Images/owl.png"},
  {"alpha" : "P",  "I" : "Images/peacock.png"},
  {"alpha" : "Q",  "I" : "Images/queen.png"},
  {"alpha" : "R",  "I" : "Images/rat.png"},
  {"alpha" : "S",  "I" : "Images/snake.png"},
  {"alpha" : "T",  "I" : "Images/toy.png"},
  {"alpha" : "U",  "I" : "Images/Umbrella.png"},
  {"alpha" : "V",  "I" : "Images/violin.png"},
  {"alpha" : "W",  "I" : "Images/watch.png"},
  {"alpha" : "X",  "I" : "Images/xylophone.png"},
  {"alpha" : "Y",  "I" : "Images/yak.png"},
  {"alpha" : "Z",  "I" : "Images/zebra.png"},

];


